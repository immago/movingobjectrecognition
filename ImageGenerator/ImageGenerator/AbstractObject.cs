﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace ImageGenerator
{
    abstract class AbstractObject : Canvas//Image
    {
        public abstract void NextFrame();
    }
}
