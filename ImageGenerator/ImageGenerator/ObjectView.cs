﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Reflection;
using System.Windows.Controls;
using System.Windows;

namespace ImageGenerator
{
    public static class ObjectView
    {
        // Make a random polygon inside the bounding rectangle.
        private static Random rand = new Random();
       
        public static Polygon GetRandomObjectView()
        {
           /*
            // Create a Polygon
            Polygon yellowPolygon = new Polygon();
            yellowPolygon.Fill = PickRandomBrush();
            yellowPolygon.StrokeThickness = 0;

            // Create a collection of points for a polygon
            PointCollection polygonPoints = new PointCollection();
            int pointsCount = rand.Next(4, 10);
            for(int i = 0; i<pointsCount; i++)
            {
                System.Windows.Point point = new System.Windows.Point(rand.Next(0, 100), rand.Next(0, 100));
                polygonPoints.Add(point);
            }
          
            // Set Polygon.Points properties
            yellowPolygon.Points = polygonPoints;

            yellowPolygon.FillRule = FillRule.Nonzero;

           return yellowPolygon;*/

            Polygon yellowPolygon = new Polygon();
            yellowPolygon.Fill = PickRandomBrush();
            yellowPolygon.StrokeThickness = 0;

            // Create a collection of points for a polygon
            PointCollection polygonPoints = new PointCollection();
            Point[] points = MakeRandomPolygon(10);
            foreach(Point point in points)
            {
                polygonPoints.Add(point);
            }
          
            // Set Polygon.Points properties
            yellowPolygon.Points = polygonPoints;

            yellowPolygon.FillRule = FillRule.Nonzero;

           return yellowPolygon;

        }

        private static Brush PickRandomBrush()
        {
        Brush result = Brushes.Transparent;
        Type brushesType = typeof(Brushes);
        PropertyInfo[] properties = brushesType.GetProperties();
        int random = rand.Next(properties.Length);
        result = (Brush)properties[random].GetValue(null, null);
        return result;
        }

        public static Point[] MakeRandomPolygon(
 int num_vertices)
        {
            // Pick random radii.
            double[] radii = new double[num_vertices];
            const double min_radius = 0.5;
            const double max_radius = 1.0;
            for (int i = 0; i < num_vertices; i++)
            {
                radii[i] = rand.NextDouble();
            }
            // Pick random angle weights.
            double[] angle_weights = new double[num_vertices];
            const double min_weight = 1.0;
            const double max_weight = 10.0;
            double total_weight = 0;
            for (int i = 0; i < num_vertices; i++)
            {
                angle_weights[i] = rand.NextDouble();
                total_weight += angle_weights[i];
            }
            // Convert the weights into fractions of 2 * Pi radians.
            double[] angles = new double[num_vertices];
            double to_radians = 2 * Math.PI / total_weight;
            for (int i = 0; i < num_vertices; i++)
            {
                angles[i] = angle_weights[i] * to_radians;
            }
            // Calculate the points' locations.
            Point[] points = new Point[num_vertices];
            float rx = 100;//bounds.Width / 2f;
            float ry = 100;//bounds.Height / 2f;
            float cx = 50;// bounds.MidX();
            float cy = 50;// bounds.MidY();
            double theta = 0;
            for (int i = 0; i < num_vertices; i++)
            {
                points[i] = new Point(
                cx + (int)(rx * radii[i] * Math.Cos(theta)),
                cy + (int)(ry * radii[i] * Math.Sin(theta)));
                theta += angles[i];
            }
            // Return the points.
            return points;
        }

    }
}
