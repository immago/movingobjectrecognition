﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.IO;
using System.Timers;
using RandomExt;

namespace ImageGenerator
{
    /// <summarssy>
    /// Hight preority:
    /// [DONE]: движение объектов задается аналитически;
    /// [DONE]: объекты имеют произвольную форму и объем;
    /// [DONE]: во время движения объект вращается вокруг своей оси;
    /// [DONE]: появление объекта случайно во времени (равномерное распределение на заданном интервале);
    /// [DONE]: задано максимальное количество объектов;
    /// [DONE]: задано соотношение сигнал/шум
    /// 
    /// [TODO] задан размер исходного изображения;
    /// [TODO]: распределение яркости объекта задается полимодальным Гауссовым распределением; 
    /// 
    /// Low preority:
    /// [TODO]: NoiseGenerator is tooooooo slooooow
    /// [TODO]: Save dialog
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        //objects
        int maxObjectsCount = 3;
        List<AbstractObject> sceneObjects = new List<AbstractObject>();

        //frame change
        Timer frameTimer = new Timer();

        //noise
        int noiseIterator = 0;
        List<ImageSource> noiseBuffer = new List<ImageSource>();
        int currentNoiseLevel = 0;

        public MainWindow()
        {
            InitializeComponent();

            //Genearate noise
            GenerateNoise();
            UpdateNoise();

            //object
            
            //if (checkBoxFormula.IsChecked == true)
            //{
                
            //    CustomMovingObject obj = new CustomMovingObject( new System.Windows.Point(0, 0), new System.Windows.Point(canvasMain.Width, canvasMain.Height), 5, textboxFormula.Text, Int32.Parse(textBoxX.Text), 1);
            //    canvasMain.Children.Add(obj);
            //    sceneObjects.Add(obj);

            //}
            //else
            //{
            //    //default: to outside

            //    InitObjects();
            //}
            InitObjects();

            //frame timer
            frameTimer.Interval = Int32.Parse(textboxFrameDelay.Text);//100.0d;
            frameTimer.Elapsed += nextFrame;
            frameTimer.Enabled = true;
            frameTimer.Start();

        }

        private void GenerateNoise()
        {
            if (currentNoiseLevel != Int32.Parse(textboxFrameNoise.Text))
            {
                currentNoiseLevel = Int32.Parse(textboxFrameNoise.Text);
                NoiseGenerator ng = new NoiseGenerator();
                noiseBuffer.Clear();
                for (int i = 0; i < 3; i++)
                    noiseBuffer.Add(ng.GenerateNoise((int)noiseImage.Width, (int)noiseImage.Height, Int32.Parse(textboxFrameNoise.Text), 0));
            }
        }

        /// <summary>
        /// Init objects
        /// </summary>
        private void InitObjects()
        {
            //clear old
            for (int i = 0; i < sceneObjects.Count; i++)
                canvasMain.Children.Remove(sceneObjects[i]);
            sceneObjects.Clear();
            
            //init
            maxObjectsCount = Int32.Parse(textboxObjectsCount.Text);
        }

        /// <summary>
        /// Rander next frame
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void nextFrame(object sender, ElapsedEventArgs e)
        {
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    UpdateNoise();

                    //obj
                    for (int i = 0; i < sceneObjects.Count; i++)
                    {
                        sceneObjects[i].NextFrame();
                        if (sceneObjects[i].Width == 0 && sceneObjects[i].Height == 0)
                            sceneObjects.Remove(sceneObjects[i]);
                    }

                    //add new
                    if (sceneObjects.Count < maxObjectsCount)
                    {
                        //chance to create new
                        for(int i = 0; i < maxObjectsCount - sceneObjects.Count; i++)
                        {
                            int chance = Int32.Parse(textboxObjectsCreateChance.Text) / (1000 / Int32.Parse(textboxFrameDelay.Text));
                            if (App.GreatRandom.Next(0, 100) <= chance)
                            {
                                //standing or moving
                                if (App.GreatRandom.Next(0, 100) <= 50)
                                {
                                    if (checkBoxFormula.IsChecked == true)
                                    {
                                        CustomMovingObject obj = new CustomMovingObject(new System.Windows.Point(0, 0), new System.Windows.Point(canvasMain.Width, canvasMain.Height), 5, textboxFormula.Text, Int32.Parse(textBoxX.Text), 1);
                                        canvasMain.Children.Add(obj);
                                        sceneObjects.Add(obj);
                                    }
                                    else
                                    {
                                        MovingObject obj = new MovingObject(new System.Windows.Point(0, 0), new System.Windows.Point(canvasMain.Width, canvasMain.Height), 0.8);
                                        canvasMain.Children.Add(obj);
                                        sceneObjects.Add(obj);
                                    }
                                }else
                                {
                                    int frames = 1000 / Int32.Parse(textboxFrameDelay.Text);
                                    StandingObject obj = new StandingObject(new System.Windows.Point(0, 0), new System.Windows.Point(canvasMain.Width, canvasMain.Height), 5 * frames, 10 * frames);
                                    canvasMain.Children.Add(obj);
                                    sceneObjects.Add(obj);
                                }

                            }
                        }
                    }

                }));
            }
            catch (Exception ex) { Console.Write("err"); }

            
        }

        /// <summary>
        /// Set next noise. Genrate noise before call this
        /// </summary>
        void UpdateNoise()
        {
            //noise
            noiseIterator++;
            if (noiseIterator >= noiseBuffer.Count) 
                noiseIterator = 0;
            
            noiseImage.Source = noiseBuffer[noiseIterator];
        }


        /// <summary>
        /// Convert from RenderTargetBitmap to png and save
        /// </summary>
        /// <param name="bmp">RenderTargetBitmap</param>
        /// <param name="filename">save path</param>
        private static void SaveRTBAsPNG(RenderTargetBitmap bmp, string filename)
        {
            var enc = new System.Windows.Media.Imaging.PngBitmapEncoder();
            enc.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(bmp));

            using (var stm = System.IO.File.Create(filename))
            {
                enc.Save(stm);
            }
        }

        /// <summary>
        /// Start save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            buttonSave.IsEnabled = false;

            int currFrame = 0;
            int framesMax = Int32.Parse(textboxSaveFramesCount.Text);

            List<RenderTargetBitmap> bitmaps = new List<RenderTargetBitmap>();

            System.Windows.Size size = new System.Windows.Size(this.Width, this.Height);

            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += (s, args) =>
            {

                canvasMain.Measure(size);
                var rtb = new RenderTargetBitmap(
                    (int)this.Width, //width 
                    (int)this.Height, //height 
                    96, //dpi x 
                    96, //dpi y 
                    PixelFormats.Pbgra32 // pixelformat 
                    );
                rtb.Render(canvasMain);
                bitmaps.Add(rtb);

                //exit
                currFrame++;
                if (currFrame > framesMax)
                {
                    dispatcherTimer.Stop();

                    //save
                    if (!Directory.Exists("saved_images"))
                        Directory.CreateDirectory("saved_images");

                    for (int i = 0; i < bitmaps.Count; i++ )
                    {
                        SaveRTBAsPNG(bitmaps[i], @"saved_images\img_" + String.Format("{0:000}", i) + ".png");
                    }
                    buttonSave.IsEnabled = true;
                    MessageBox.Show("Saved comlite!");
                }
            };
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, Int32.Parse(textboxSaveFramesDelay.Text));
            dispatcherTimer.Start();

        }

        /// <summary>
        /// Update render params
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpadte_Click(object sender, RoutedEventArgs e)
        {
            GenerateNoise();

            frameTimer.Stop();
            frameTimer.Interval = Int32.Parse(textboxFrameDelay.Text);
            frameTimer.Start();

            InitObjects();
        }

    }
}
