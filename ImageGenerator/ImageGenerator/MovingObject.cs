﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace ImageGenerator
{
    // Инкапсулирует логику движения
    class MovingObject : AbstractObject
    {
        Point destenetionPoint;
        double speed;
        Point minValues; 
        Point maxValues;

        //rotate
        bool rotateClockwice;
        double rotateSpeed;
        double currentAngle = 0;
        
        //scale
        double minScale;
        double currentScale = 1;
        bool isScaleUp = false;

        public MovingObject(Point minValues, Point maxValues, double speed)
        {
            //Random rand = new Random();
            Random rand = App.GreatRandom;

            this.minValues = minValues;
            this.maxValues = maxValues;
            this.speed = speed;

            //rotation config
            this.RenderTransformOrigin = new Point(0.5, 0.5);
            rotateSpeed = rand.Next(1, 20);
            rotateClockwice = (50 > rand.Next(0, 100)) ? true : false;

            //scale
            minScale = rand.NextDouble() + 0.2;

            //this.destenetionPoint = new Point( rand.Next((int)minValues, (int)maxValues.X), rand.Next((int)minValues.Y, (int)maxValues.Y));

            this.Margin = new Thickness(rand.Next((int)minValues.X, (int)maxValues.X), rand.Next((int)minValues.Y, (int)maxValues.Y), 0, 0);
            this.Children.Add(ObjectView.GetRandomObjectView());
            //this.Source = new BitmapImage(new Uri("image.png", UriKind.Relative));
            this.Width = 100;
            this.Height = 100;


            this.destenetionPoint = new Point( (( rand.Next(100) > 50 ) ? -minValues.X - 50 : maxValues.X + 50),
                                               (( rand.Next(100) > 50 ) ? -minValues.Y - 50 : maxValues.Y + 50));

        }

        public override void NextFrame()
        {

            //Moving
            double topStep;
            if ((destenetionPoint.Y - this.Margin.Top) > 0)
                topStep = Math.Max((destenetionPoint.Y - this.Margin.Top) / 100 * speed, speed);
            else
                topStep = Math.Min((destenetionPoint.Y - this.Margin.Top) / 100 * speed, -speed);


            double leftStep;
            if ((destenetionPoint.Y - this.Margin.Top) > 0)
                leftStep = Math.Max((destenetionPoint.Y - this.Margin.Left) / 100 * speed, speed);
            else
                leftStep = Math.Min((destenetionPoint.Y - this.Margin.Left) / 100 * speed, -speed);


            double top = this.Margin.Top + topStep;
            double left = this.Margin.Left + leftStep;

            this.Margin = new Thickness(left,top, 0, 0);


            //Transform

            //rotate
            RotateTransform objectRotateTransform = new RotateTransform();
            if(rotateClockwice)
                currentAngle += rotateSpeed;
            else
                currentAngle -= rotateSpeed;

            objectRotateTransform.Angle = currentAngle;

            //scale
            ScaleTransform objectScaleTransform = new ScaleTransform();

            if(isScaleUp)
            {
                currentScale += 0.04;
                if (currentScale > 1)
                    isScaleUp = false;

            }else
            {
                currentScale -= 0.04;
                if (currentScale < minScale)
                    isScaleUp = true;
            }

            objectScaleTransform.ScaleX = currentScale;
            objectScaleTransform.ScaleY = currentScale;

            //setting up render
            TransformGroup myTransformGroup = new TransformGroup();
            myTransformGroup.Children.Add(objectRotateTransform);
            myTransformGroup.Children.Add(objectScaleTransform);

            this.RenderTransform = myTransformGroup;

            //Out
            if ((this.Margin.Top + this.Height / 2 >= maxValues.Y) || (this.Margin.Top + this.Height / 2 <= minValues.Y) || (this.Margin.Left + this.Width / 2 >= maxValues.X) || (this.Margin.Left + +this.Width / 2 <= minValues.X))
            {
                this.Width = 0;
                this.Height = 0;

                if (this.Parent != null)
                {
                    ((Canvas)(this.Parent)).Children.Remove(this);
                }
            }
        }

    }
}
