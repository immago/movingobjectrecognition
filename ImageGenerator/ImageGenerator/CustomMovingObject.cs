﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Media;

namespace ImageGenerator
{
    class CustomMovingObject: AbstractObject
    {

        string formula;
        double startX;
        double x;
        double step;
        double speed;
        Point minValues;
        Point maxValues;


        //rotate
        bool rotateClockwice;
        double rotateSpeed;
        double currentAngle = 0;

        //scale
        double minScale;
        double currentScale = 1;
        bool isScaleUp = false;

        int yRand;


        public CustomMovingObject(Point minValues, Point maxValues, double speed, string formula, double startX = 0, double step = 1)
        {
            this.formula = formula;
            this.startX = startX;
            this.step = step;
            x = startX;

            this.minValues = minValues;
            this.maxValues = maxValues;
            this.speed = speed;

            this.Children.Add(ObjectView.GetRandomObjectView());
            //this.Source = new BitmapImage(new Uri("image.png", UriKind.Relative));
            this.Width = 100;
            this.Height = 100;

            yRand = App.GreatRandom.Next(-200, 200);

            //rotation config
            this.RenderTransformOrigin = new Point(0.5, 0.5);
            rotateSpeed = App.GreatRandom.Next(1, 20);
            rotateClockwice = (50 > App.GreatRandom.Next(0, 100)) ? true : false;

            //scale
            minScale = App.GreatRandom.NextDouble() + 0.2;

            //start pos
            ExpressionParser ep = new ExpressionParser();
            double y = ep.parse(formula, x);
            this.Margin = new Thickness(x, y, 0, 0);

        }
        public override void NextFrame()
        {
            x += step;
            ExpressionParser ep = new ExpressionParser();
            double y = ep.parse(formula, x);

            this.Margin = new Thickness(x * speed, y + yRand, 0, 0);

            //Transform

            //rotate
            RotateTransform objectRotateTransform = new RotateTransform();
            if (rotateClockwice)
                currentAngle += rotateSpeed;
            else
                currentAngle -= rotateSpeed;

            objectRotateTransform.Angle = currentAngle;

            //scale
            ScaleTransform objectScaleTransform = new ScaleTransform();

            if (isScaleUp)
            {
                currentScale += 0.04;
                if (currentScale > 1)
                    isScaleUp = false;

            }
            else
            {
                currentScale -= 0.04;
                if (currentScale < minScale)
                    isScaleUp = true;
            }

            objectScaleTransform.ScaleX = currentScale;
            objectScaleTransform.ScaleY = currentScale;

            //setting up render
            TransformGroup myTransformGroup = new TransformGroup();
            myTransformGroup.Children.Add(objectRotateTransform);
            myTransformGroup.Children.Add(objectScaleTransform);

            this.RenderTransform = myTransformGroup;

            if ((this.Margin.Top + this.Height / 2 >= maxValues.Y) || (this.Margin.Top + this.Height / 2 <= minValues.Y) || (this.Margin.Left + this.Width / 2 >= maxValues.X) || (this.Margin.Left + +this.Width / 2 <= minValues.X))
            {
                this.Width = 0;
                this.Height = 0;

                if (this.Parent != null)
                {
                    ((Canvas)(this.Parent)).Children.Remove(this);
                }
            }
        }
    }
}
