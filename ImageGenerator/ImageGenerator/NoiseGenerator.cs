﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

using RandomExt;

namespace ImageGenerator
{
    class NoiseGenerator
    {
        public BitmapImage GenerateNoise(int width, int height, int whiteMax = 255, int whiteMin = 0)
        {
            Bitmap finalBmp = new Bitmap(width, height);
            Random r = new Random();

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    
                    int num = r.Next(whiteMin, whiteMax);
                    if (r.Next(0, 100) > 50)
                    {
                        finalBmp.SetPixel(x, y, System.Drawing.Color.FromArgb(num, 255, 255, 255)); //white
                    }else
                    {
                        finalBmp.SetPixel(x, y, System.Drawing.Color.FromArgb(num, 0, 0, 0)); //black
                    }
                    
                }
            }

            return BitmapToBitmapImage(finalBmp);
        }


        BitmapImage BitmapToBitmapImage(Bitmap src)
        {
            MemoryStream ms = new MemoryStream();
            src.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Position = 0;
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();

            return bi;
        }


        public Bitmap GenerateNoiseGaussan(int width, int height, int whiteMax = 255, int whiteMin = 0)
        {
            Bitmap finalBmp = new Bitmap(width, height);
            Random r = new Random();

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int num = ((int)Math.Abs(r.NextGaussian())) * 25;

                    finalBmp.SetPixel(x, y, System.Drawing.Color.FromArgb(255, num, num, num));
                }
            }

            return finalBmp;
        }
    }

}
