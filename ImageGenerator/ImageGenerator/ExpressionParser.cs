﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NCalc;

namespace ImageGenerator
{
    class ExpressionParser
    {
        public double parse(string str, double x)
        {
            Expression e = new Expression(str);

            e.Parameters["Pi2"] = new Expression("Pi * Pi");
            e.Parameters["X"] = x;
            e.Parameters["x"] = x;

            e.EvaluateParameter += delegate(string name, ParameterArgs args)
            {
                if (name == "Pi")
                    args.Result = 3.14;
            };

            double result = (double)e.Evaluate();
            return result;
            
        }

    }
}
