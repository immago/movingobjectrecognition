﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace ImageGenerator
{
    class StandingObject: AbstractObject
    {

        private int frameCount;
        private int endFrame;

        public StandingObject(Point minValues, Point maxValues, int minFrames, int maxFrames)
        {
            Random rand = App.GreatRandom;
           
            this.Margin = new Thickness(rand.Next((int)minValues.X, (int)maxValues.X), rand.Next((int)minValues.Y, (int)maxValues.Y), 0, 0);
            this.Children.Add(ObjectView.GetRandomObjectView());
            //this.Source = new BitmapImage(new Uri("stand.jpg", UriKind.Relative));
            this.Width = 200;
            this.Height = 200;

            this.endFrame = rand.Next(minFrames, maxFrames);
        }

        public override void NextFrame()
        {
            frameCount++;
            if(frameCount>=endFrame)
            {
                this.Width = 0;
                this.Height = 0;

                if (this.Parent != null)
                {
                    ((Canvas)(this.Parent)).Children.Remove(this);
                }
            }
        }
    }
}
