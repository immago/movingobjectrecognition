﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media.Imaging;

namespace ImmagoImageDetection
{
    class FilesReader
    {
        public List<BitmapImage> getImagesFromFolder(string path, int outputWidth, int outputHeight)
        {
            List<BitmapImage> result = new List<BitmapImage>();

            List<string> names = Directory.GetFiles(path, "*.png", SearchOption.AllDirectories).ToList();
            

            foreach(string name in names)
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.DecodePixelWidth = outputWidth;
                image.DecodePixelHeight = outputHeight;
                image.UriSource = new Uri(name);
                image.EndInit();

                result.Add(image);
            }

            return result;
        }
    }
}
