﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace ImmagoImageDetection
{
    class MovingDetection
    {
        public int Error = 3;

        public List<bool> IsObjectsMoving(List<Rectangle> firstRects, List<Rectangle> nextRects )
        {
            List<bool> result = new List<bool>();

            int count = Math.Min(firstRects.Count, nextRects.Count);

            for(int i = 0; i < count; i++)
            {
                Rectangle r1 = firstRects[i];
                Rectangle r2 = nextRects[i];

                if (
                    (Math.Abs(r1.Margin.Left - r2.Margin.Left) <= Error) &&
                    (Math.Abs(r1.Margin.Top - r2.Margin.Top) <= Error) &&
                    (Math.Abs(r1.Width - r2.Width) <= Error) &&
                    (Math.Abs(r1.Height - r2.Height) <= Error)
                )
                    result.Add(false);
                else
                    result.Add(true);
               
            }

            return result;

        }

    }
}
