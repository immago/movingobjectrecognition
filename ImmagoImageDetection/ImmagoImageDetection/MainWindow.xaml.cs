﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;

namespace ImmagoImageDetection
{
    /// <summary>
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        List<System.Windows.Shapes.Rectangle> prevPoints;
        System.Windows.Threading.DispatcherTimer frameDispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        int bgRedMin = 0, bgRedMax = 0, bgGreenMin = 0, bgGreenMax = 0, bgBlueMin = 0, bgBlueMax = 0;


        int currFrame;
        List<BitmapImage> originalImages;
        List<BitmapImage> imagesStep2;
        List<BitmapImage> imagesStep1;


        WindowLoading loadingWindow = new WindowLoading();

        public MainWindow()
        {
            InitializeComponent();
        }

        void ParseBackgroundConfig()
        {
            bgRedMin = Int32.Parse(textboxBgRedMin.Text);
            bgRedMax = Int32.Parse(textboxBgRedMax.Text);
            bgGreenMin = Int32.Parse(textboxBgGreenMin.Text);
            bgGreenMax = Int32.Parse(textboxBgGreenMax.Text);
            bgBlueMin = Int32.Parse(textboxBgBlueMin.Text);
            bgBlueMax = Int32.Parse(textboxBgBlueMax.Text);
        }

        void UpdateObjectRects()
        {
            System.Drawing.Bitmap srcimg = ImageWpfToGDI(originalImages[currFrame]);//(imageSource.Source);

            ImageDetection idet = new ImageDetection(bgRedMin, bgRedMax, bgGreenMin, bgGreenMax, bgBlueMin, bgBlueMax);
            List<System.Windows.Shapes.Rectangle> points = idet.ProcessImage(srcimg);
            
            MovingDetection md = new MovingDetection();

            canvasSource.Children.Clear();

            if (prevPoints != null)
            {
                List<bool> movingObjects = md.IsObjectsMoving(prevPoints, points);
                for(int i = 0; i< movingObjects.Count; i++)
                {
                    Rectangle r = points[i];
                    if (movingObjects[i])
                        r.Stroke = Brushes.Green;
                    else
                        r.Stroke = Brushes.Red;

                    canvasSource.Children.Add(r);
                }
            }
            prevPoints = points;
        }

        private System.Drawing.Bitmap ImageWpfToGDI(System.Windows.Media.ImageSource imageSource)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            var encoder = new System.Windows.Media.Imaging.BmpBitmapEncoder();
            encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(imageSource as System.Windows.Media.Imaging.BitmapSource));
            encoder.Save(ms);
            ms.Flush();
            return new System.Drawing.Bitmap(System.Drawing.Image.FromStream(ms));
            
        }

        BitmapImage BitmapToBitmapImage(System.Drawing.Bitmap src)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            src.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Position = 0;
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();

            return bi;
        }


        private void buttonOpen_Click(object sender, RoutedEventArgs e)
        {
            

            System.Windows.Forms.FolderBrowserDialog folderSelector = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = folderSelector.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                
                FilesReader fl = new FilesReader();
                originalImages = fl.getImagesFromFolder(folderSelector.SelectedPath, (int)canvasSource.Width, (int)canvasSource.Height);

                StartProcessing();
            }

        }

        private void StartProcessing()
        {
            loadingWindow.Show();

            ParseBackgroundConfig();

            //process step 1
            ImageDetection idet = new ImageDetection(bgRedMin, bgRedMax, bgGreenMin, bgGreenMax, bgBlueMin, bgBlueMax);
            imagesStep1 = new List<BitmapImage>();
            foreach (BitmapImage bi in originalImages)
            {
                imagesStep1.Add(BitmapToBitmapImage(idet.FilterBackground(ImageWpfToGDI(bi))));
            }

            //step 2 edge points
            imagesStep2 = new List<BitmapImage>();
            foreach (BitmapImage bi in originalImages)
            {
                imagesStep2.Add(BitmapToBitmapImage(idet.GetBoundaryPoints(ImageWpfToGDI(bi))));
            }


            currFrame = 0;

            //frame timer
            frameDispatcherTimer.Stop();
            frameDispatcherTimer.IsEnabled = false;
            frameDispatcherTimer = new System.Windows.Threading.DispatcherTimer();

            frameDispatcherTimer.Tick += (s, args) =>
            {
                currFrame++;
                if (currFrame >= originalImages.Count)
                    currFrame = 0;

                ImageUpdateByRadioButtonClick();

                UpdateObjectRects();


            };
            frameDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, Int32.Parse(textBoxFrameDelay.Text));
            frameDispatcherTimer.Start();

            loadingWindow.Hide();
        }

        private void ImageUpdateByRadioButtonClick()
        {
            if (originalImages != null && imagesStep1 != null && imagesStep2 != null)
            {
                if (radioButtonOriginal.IsChecked == true)
                {
                    imageSource.Source = originalImages[currFrame];
                }

                if (radioButtonStep1.IsChecked == true)
                {

                    imageSource.Source = imagesStep1[currFrame];
                }

                if (radioButtonStep2.IsChecked == true)
                {
                    imageSource.Source = imagesStep2[currFrame];
                }
            }
        }

        private void buttonUpdate_Click(object sender, RoutedEventArgs e)
        {
            StartProcessing();
        }

        private void buttonPause_Click(object sender, RoutedEventArgs e)
        {

            frameDispatcherTimer.IsEnabled = !frameDispatcherTimer.IsEnabled;
        }

        private void radioButtonUpdate(object sender, RoutedEventArgs e)
        {
            ImageUpdateByRadioButtonClick();
        }

        private void AppClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            frameDispatcherTimer.IsEnabled = false;
            frameDispatcherTimer.Stop();
            Application.Current.Shutdown();
        }

    }
}
