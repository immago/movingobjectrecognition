﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;

// AForge.NET framework
// http://www.aforgenet.com/framework/
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;

namespace ImmagoImageDetection
{
    class ImageDetection
    {
        int bgRedMin = 0, bgRedMax = 64, bgGreenMin = 0, bgGreenMax = 64, bgBlueMin = 0, bgBlueMax = 64;

        public ImageDetection(int bgRedMin = 0, int bgRedMax = 64, int bgGreenMin = 0, int bgGreenMax = 64, int bgBlueMin = 0, int bgBlueMax = 64)
        {
            this.bgRedMin = bgRedMin;
            this.bgRedMax = bgRedMax;
            this.bgGreenMin = bgGreenMin;
            this.bgGreenMax = bgGreenMax;
            this.bgBlueMin = bgBlueMin;
            this.bgBlueMax = bgBlueMax;
        }
        public List<System.Windows.Shapes.Rectangle> ProcessImage(Bitmap bitmap)
        {

            List<System.Windows.Shapes.Rectangle> result = new List<System.Windows.Shapes.Rectangle>();

            // lock image
            BitmapData bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, bitmap.PixelFormat);

            // step 1 - turn background to black
            ColorFiltering colorFilter = new ColorFiltering();

            colorFilter.Red = new IntRange(bgRedMin, bgRedMax);
            colorFilter.Green = new IntRange(bgGreenMin, bgGreenMax);
            colorFilter.Blue = new IntRange(bgBlueMin, bgBlueMax);
            colorFilter.FillOutsideRange = false;

            colorFilter.ApplyInPlace(bitmapData);

            // step 2 - locating objects
            BlobCounter blobCounter = new BlobCounter();

            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 5;
            blobCounter.MinWidth = 5;

            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            bitmap.UnlockBits(bitmapData);

            // step 3 - check objects
           
            Rectangle[] rects = blobCounter.GetObjectsRectangles();
            foreach(Rectangle rect in rects)
            {
                System.Windows.Shapes.Rectangle p = new System.Windows.Shapes.Rectangle()
                {
                    Width = rect.Width,
                    Height = rect.Height,
                    Margin = new System.Windows.Thickness(rect.X,rect.Y,0,0),
                    Stroke = System.Windows.Media.Brushes.Red,
                    StrokeThickness = 3
                };
                result.Add(p);
            }

            return result;

        }

        public Bitmap FilterBackground(Bitmap bitmap)
        {
            // lock image
            BitmapData bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, bitmap.PixelFormat);

            // turn background to black
            ColorFiltering colorFilter = new ColorFiltering();

            colorFilter.Red = new IntRange(bgRedMin, bgRedMax);
            colorFilter.Green = new IntRange(bgGreenMin, bgGreenMax);
            colorFilter.Blue = new IntRange(bgBlueMin, bgBlueMax);
            colorFilter.FillOutsideRange = false;

            colorFilter.ApplyInPlace(bitmapData);
            bitmap.UnlockBits(bitmapData);

            return bitmap;

        }

        public Bitmap GetBoundaryPoints(Bitmap bitmap)
        {
             // lock image
            BitmapData bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, bitmap.PixelFormat);

            // step 1 - turn background to black
            ColorFiltering colorFilter = new ColorFiltering();

            colorFilter.Red = new IntRange(bgRedMin, bgRedMax);
            colorFilter.Green = new IntRange(bgGreenMin, bgGreenMax);
            colorFilter.Blue = new IntRange(bgBlueMin, bgBlueMax);
            colorFilter.FillOutsideRange = false;

            colorFilter.ApplyInPlace(bitmapData);

            // step 2 - locating objects
            BlobCounter blobCounter = new BlobCounter();

            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 5;
            blobCounter.MinWidth = 5;

            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            bitmap.UnlockBits(bitmapData);

            // step 3 - check objects

            Bitmap resultBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            Graphics g = Graphics.FromImage(resultBitmap);
            g.Clear(Color.Black);

            foreach (Blob blob in blobs)
            {
                foreach(IntPoint ip in blobCounter.GetBlobsEdgePoints(blob))
                {
                    g.FillEllipse(Brushes.White, ip.X, ip.Y, 2, 2);
                }
            }

            return resultBitmap;
        }
    }
}
